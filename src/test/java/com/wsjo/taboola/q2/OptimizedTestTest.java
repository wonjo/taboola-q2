package com.wsjo.taboola.q2;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class OptimizedTestTest {
	
	OptimizedTest t;
	
	@Before
	public void setup() {
		List<Integer> numbers = Arrays.asList(1, 2, 55, 88, 55, 33, 2, 88, 55);
		List<String> strings = Arrays.asList("a", "b", "c", "a", "a", "window", "linux", "ios", "linux", "ubuntu");
		t = new OptimizedTest(new Date(), "Bob", numbers, strings);
	}
	
	@Test
	public void equalTest() {
		List<Integer> numbers = Arrays.asList(1, 2, 55, 88, 55, 33, 2, 88, 55);
		List<String> strings = Arrays.asList("a", "b", "c", "a", "a", "window", "linux", "ios", "linux", "ubuntu");
		OptimizedTest tmp = new OptimizedTest(new Date(), "Bob", numbers, strings);
		Assert.assertTrue(t.equals(tmp));
	}
	
	@Test
	public void noEqualTest() {
		List<Integer> numbers = Arrays.asList(1, 2, 55, 88, 55, 33, 2, 88, 557);
		List<String> strings = Arrays.asList("a", "b", "c", "a", "a", "window", "linux", "ios", "linux");
		OptimizedTest tmp = new OptimizedTest(new Date(), "Bob", numbers, strings);
		Assert.assertFalse(t.equals(tmp));
	}

	@Test
	public void removeStringTest() {
		t.removeString("linux");
		Assert.assertTrue(t.containsString("linux"));
		t.removeString("linux");
		Assert.assertFalse(t.containsString("linux"));
		
	}
	
	@Test
	public void containsNumberTest() {
		Assert.assertTrue(t.containsNumber(1));
		Assert.assertTrue(t.containsNumber(2));
		Assert.assertTrue(t.containsNumber(88));
		Assert.assertTrue(t.containsNumber(55));
		Assert.assertTrue(t.containsNumber(33));
		Assert.assertFalse(t.containsNumber(125));
		Assert.assertFalse(t.containsNumber(1000000));
	}
	
	@Test
	public void isHistoricTest() throws InterruptedException {
		Thread.sleep(1000); // one second delay
		Assert.assertTrue(t.isHistoric());
	}
}
