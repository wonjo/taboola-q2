package com.wsjo.taboola.q2;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//class name change to prevent conflict with junit Test interface
public class OptimizedTest {
	
	private Date time;  //member data naming java convention
	private String name;
	private Map<Integer, Integer> numbers;
	private Map<String, Integer> strings;
	
	// a constructor accepting List and it converts to hashmap for memory and performance optimization
	public OptimizedTest(Date time, String name, List<Integer> numbers, List<String> strings) {
		this.time = time;  // Java convention naming
		this.name = name;
		this.numbers = new HashMap<Integer, Integer>();
		this.strings = new HashMap<String, Integer>();
		for(Integer n: numbers) {
			if(this.numbers.get(n) == null) {
				this.numbers.put(n, 1);
			} else {
				this.numbers.put(n, this.numbers.get(n) + 1);
			}
		}
		
		for(String s: strings) {
			if(this.strings.get(s) == null) {
				this.strings.put(s, 1);
			} else {
				this.strings.put(s, this.strings.get(s) + 1);
			}
		}
	}
	
	public OptimizedTest(Date time, String name, Map<Integer, Integer> numbers, Map<String, Integer> strings) {
		this.time = time;
		this.name = name;
		this.numbers = numbers;
		this.strings = strings;
	}

	// missing hashcode generation
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((numbers == null) ? 0 : numbers.hashCode());
		result = prime * result + ((strings == null) ? 0 : strings.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	// equals method updated to handle null objecs and class type.
	// compare other missing member data (numbers, strings, and time)
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OptimizedTest other = (OptimizedTest) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (numbers == null) {
			if (other.numbers != null)
				return false;
		} else if (!numbers.equals(other.numbers))
			return false;
		if (strings == null) {
			if (other.strings != null)
				return false;
		} else if (!strings.equals(other.strings))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}

	// toString updated to include other member data and formatting improvements
	@Override
	public String toString() {
		return "OptimizedTest [time=" + time + ", name=" + name + ", numbers=" + numbers + ", strings=" + strings + "]";
	}
	
	// by using hash map performance has improved
	public void removeString(String str) {
		Integer counter = strings.get(str);
		if(counter != null) {
			if(counter == 1)
				strings.remove(str);
			else
				strings.put(str, counter - 1);
		}
	}
	
	// by using hash map performance has improved
	public boolean containsNumber(int number) {
		return numbers.get(number) != null;
	}
	
	public boolean isHistoric() {
		return time.before(new Date());
	}
	
	// generated for unit test purpose
	public boolean containsString(String str) {
		return strings.get(str) != null;
	}
}
